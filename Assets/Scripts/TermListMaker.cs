﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class TermListMaker {
    [MenuItem("Assets/Create/New TermList")]
    public static void CreateMyAsset() {
        var asset = ScriptableObject.CreateInstance<TermList>();

        AssetDatabase.CreateAsset(asset, "Assets/NewTermList.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
