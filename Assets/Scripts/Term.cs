﻿using UnityEngine;
[System.Serializable]
public class Term {
    public string Word;
    public Sprite Sprite;
    public Term() {
        Word = "";
        Sprite = null;
    }

    public Term(string wordInSillables, Sprite sprite) {
        Word = wordInSillables;
        Sprite = sprite;
    }

}
