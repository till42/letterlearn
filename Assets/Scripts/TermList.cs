﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TermList : ScriptableObject {
    //Singleton pattern
    private static TermList _instance;
    private static TermList instance {
        get {
            if (_instance == null) {
                _instance = Resources.Load("DefaultTermList") as TermList;
            }
            if (_instance == null)
                Debug.LogError("No DefaultTermList found!");
            return _instance;
        }
    }


    public List<Term> Terms;

    public static List<Term> AllTerms { get { return instance.Terms; } }

    public static void RefreshFiles() {
        //get all sprites in the pictures folder
        Sprite[] sprites = Resources.LoadAll<Sprite>("Pictures");

        //clear the list of terms
        instance.Terms = new List<Term>();

        //go through all sprites and a term for it in the list of terms
        foreach (Sprite sprite in sprites) {
            string word = sprite.name.Replace("-", string.Empty);
            Term t = new Term(word, sprite);
            instance.Terms.Add(t);
        }
#if UNITY_EDITOR
        EditorUtility.SetDirty(instance);
#endif
    }

}
