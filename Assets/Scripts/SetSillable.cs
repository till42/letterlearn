﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetSillable : MonoBehaviour {

    private Text textComponent;

    void Start() {

        //get the text component 
        textComponent = GetComponentInChildren<Text>();

        //refresh the hud
        OnRefreshHUD();
    }

    public void OnRefreshHUD() {
        textComponent.text = Terms.CurrentSillable;
    }
}
