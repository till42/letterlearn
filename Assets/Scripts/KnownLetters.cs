﻿using UnityEngine;
using System.Collections.Generic;

public class KnownLetters : MonoBehaviour {

    //Singleton Pattern
    private static KnownLetters _instance;
    private static KnownLetters instance { get { if (_instance == null) _instance = FindObjectOfType<KnownLetters>(); return _instance; } }

    public List<char> Letters;

    public static readonly List<char> Consonants = new List<char> {
        'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P','Q','R','S','T','V','W','X','Z'
    };

    public static readonly List<char> Vocals = new List<char> {
        'A', 'E', 'I', 'O', 'U', 'Ä', 'Ö', 'Ü','Y'
    };

    public static List<char> KnownConsonants {
        get {
            //create return list
            List<char> known = new List<char>();
            //go through all know letters
            foreach (char c in instance.Letters) {
                //if the letter is a consonant
                if (Consonants.Contains(c))
                    //...add it to the return list
                    known.Add(c);
            }
            //return the list
            return known;
        }
    }

    public static List<char> KnownVocals {
        get {
            //create return list
            List<char> known = new List<char>();
            //go through all know letters
            foreach (char c in instance.Letters) {
                //if the letter is a vocal
                if (Vocals.Contains(c))
                    //...add it to the return list
                    known.Add(c);
            }
            //return the list
            return known;
        }
    }

    public static string GetRandomKnownSillable() {
        string sillable;

        //break if no
        if (KnownConsonants.Count <= 0)
            return "NE";
        if (KnownVocals.Count <= 0)
            return "NE";
        if (instance.Letters.Count <= 0)
            return "NE";

        //get a random true or false
        bool isFirstLetterConsonant = Random.Range(0, 2) == 0 ? true : false;

        //get a random consonant
        char consonant = KnownConsonants[Random.Range(0, KnownConsonants.Count)];

        //get a random vocal
        char vocal = KnownVocals[Random.Range(0, KnownVocals.Count)];

        if (isFirstLetterConsonant)
            sillable = consonant.ToString() + vocal.ToString();
        else
            sillable = vocal.ToString() + consonant.ToString();

        //return it, if there are terms
        if (Terms.TermExistsWithSillable(sillable))
            return sillable;

        //otherwise give it another try
        return GetRandomKnownSillable();
    }

}
