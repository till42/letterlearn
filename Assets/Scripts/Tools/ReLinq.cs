﻿using System.Collections.Generic;
using System;

public static class ReLinq {
    public static bool Any<T>(this List<T> list) {
        if (list == null)
            return false;
        if (list.Count <= 0)
            return false;
        return true;
    }

    public static int Max<T>(
        this List<T> source,
        Func<T, int> selector
    ) {
        int retVal = int.MinValue;
        foreach (T element in source) {
            int val = selector(element);
            if (val > retVal)
                retVal = val;
        }
        return retVal;
    }

    public static float Max<T>(
    this List<T> source,
    Func<T, float> selector
    ) {
        float retVal = float.MinValue;
        foreach (T element in source) {
            float val = selector(element);
            if (val > retVal)
                retVal = val;
        }
        return retVal;
    }

    public static float Min<T>(
    this List<T> source,
    Func<T, float> selector
    ) {
        float retVal = float.MaxValue;
        foreach (T element in source) {
            float val = selector(element);
            if (val < retVal)
                retVal = val;
        }
        return retVal;
    }

    public static T First<T>(this List<T> list) {
        if (list.Count <= 0)
            return default(T);

        return list[0];
    }
    public static T First<T>(this List<T> source, Func<T, bool> predicate) {
        if (source.Count <= 0)
            return default(T);

        foreach (T element in source) {
            if (predicate(element))
                return element;
        }
        return default(T);
    }
    public static T Last<T>(this List<T> list) {
        int count = list.Count;
        if (count <= 0)
            return default(T);

        return list[count - 1];
    }


    public static bool Contains(this Array a, object val) {
        return Array.IndexOf(a, val) != -1;
    }

    public static bool Contains<T>(
    this List<T> source,
    T value,
    IEqualityComparer<T> comparer
    ) {
        foreach (T element in source) {
            if (comparer.Equals(value, element))
                return true;
        }
        return false;
    }

    public static List<T> Where<T>(this List<T> source, Func<T, bool> predicate) {
        List<T> list = new List<T>();
        foreach (T element in source) {
            if (predicate(element)) {
                list.Add(element);
            }
        }
        return list;
    }

    public static List<T> ToList<T>(this Array a) {
        List<T> list = new List<T>();
        for (int i = 0; i < a.Length; i++) {
            list.Add((T)a.GetValue(i));
        }
        return list;
    }

    public static T[][] To2DimensionalArray<T>(this List<List<T>> twoDimensionalList)
    //convert a 2-dimensional List into a 2-dimensional array
    {
        //create the array which is returned, size of the first dimension is set
        var array = new T[twoDimensionalList.Count][];

        //go through each list entry on the first dimension
        for (int x = 0; x < twoDimensionalList.Count; x++) {
            //save the list into a variable for easier acces
            List<T> subList = twoDimensionalList[x];

            //define the array size in the second dimension
            array[x] = new T[subList.Count];

            //go through each list entry on the second dimension
            for (int y = 0; y < subList.Count; y++) {
                //copy the list element into the array element
                array[x][y] = subList[y];
            }
        }

        //return the arry
        return array;
    }

    public static List<List<T>> To2DimensionalList<T>(this IList<IList<T>> twoDimensionalIList) {
        var list = new List<List<T>>();

        foreach (IList<T> subIList in twoDimensionalIList) {
            var listLine = new List<T>();
            foreach (T entry in subIList) {
                listLine.Add(entry);
            }
            list.Add(listLine);
        }
        return list;
    }


    public static List<T> Clone<T>(this List<T> l) {
        var value = new List<T>();
        foreach (T t in l) {
            value.Add(t);
        }
        return value;
    }
}
