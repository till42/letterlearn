﻿using UnityEngine;
using System.Collections.Generic;

public class Terms : MonoBehaviour {

    //Singleton Pattern
    private static Terms _instance;
    private static Terms instance { get { if (_instance == null) _instance = FindObjectOfType<Terms>(); return _instance; } }

    [SerializeField]
    private List<Term> _sentTerms = new List<Term>();
    private static List<Term> sentTerms { get { return instance._sentTerms; } set { instance._sentTerms = value; } }

    private static List<Term> _poolOfTerms;
    private static List<Term> poolOfTerms {
        get {
            if (_poolOfTerms == null) _poolOfTerms = TermList.AllTerms.Clone();
            return _poolOfTerms;
        }
    }

    public static int correctAnswerID = int.MinValue;


    //returns a random Term, which is known by the kid.
    //it will always give the same term for the same index
    public static Term GetRandomTerm(int index, out bool isCorrect) {

        //set the correct answer, if not yet set
        if (correctAnswerID == int.MinValue) {
            correctAnswerID = Random.Range(0, Answer.NumberOfPossibleAnswers);
        }

        //return wrong answer, if the index is wrong
        if (index != correctAnswerID) {
            isCorrect = false;
            return getWrongAnswer();
        }

        //otherwise return the correct answer
        isCorrect = true;
        return getCorrectAnswer();
    }

    private static Term getWrongAnswer() {
        if (poolOfTerms == null || poolOfTerms.Count <= 0)
            return new Term();

        int i = Random.Range(0, poolOfTerms.Count);
        Term t = poolOfTerms[i];

        //remove the found term from the pool
        poolOfTerms.Remove(t);
        return t;
    }

    private static Term getCorrectAnswer() {
        int i = Random.Range(0, TermsWithCurrentSillable.Count);
        return TermsWithCurrentSillable[i];
    }

    public static List<Term> TermsWithSillable(string sillable) {
        List<Term> termList = new List<Term>();

        //go through the whole list
        foreach (Term t in TermList.AllTerms) {
            //only add terms which contain the sillable
            if (t.Word.ToUpper().Contains(sillable.ToUpper()))
                termList.Add(t);
        }
        return termList;
    }

    [SerializeField]
    private List<Term> _termsWithCurrentSillable = null;
    public static List<Term> TermsWithCurrentSillable {
        get {
            if (instance._termsWithCurrentSillable == null || instance._termsWithCurrentSillable.Count <= 0)
                instance._termsWithCurrentSillable = TermsWithSillable(CurrentSillable);
            return instance._termsWithCurrentSillable;
        }
    }

    [SerializeField]
    private string currentSillable;
    public static string CurrentSillable {
        get {
            if (string.IsNullOrEmpty(instance.currentSillable))
                instance.currentSillable = KnownLetters.GetRandomKnownSillable();
            return instance.currentSillable;
        }
    }


    public static bool TermExistsWithSillable(string sillable) {
        //go through each term in all terms
        foreach (Term t in TermList.AllTerms) {
            //if the word of the term contains the sillable (both in upper case)...
            if (t.Word.ToUpper().Contains(sillable.ToUpper()))
                //...yes, it exists
                return true;
        }

        //otherwise not
        return false;
    }
}
