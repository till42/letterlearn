﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Answer : MonoBehaviour {

    private Term term;
    public Image Image;
    public Image PanelImage;
    private Text textComponent;
    [SerializeField]
    private int index;
    private bool isCorrect;

    private static int overall_index = 0;
    public static int NumberOfPossibleAnswers = int.MinValue;

    public static List<Answer> AllAnswers = new List<Answer>();

    void Awake() {
        //set the number of possible answers, if not set
        if (NumberOfPossibleAnswers == int.MinValue) {
            NumberOfPossibleAnswers = FindObjectsOfType<Answer>().Length;
        }

        AllAnswers.Add(this);
    }


    // Use this for initialization
    void Start() {
        //set own index and increase it
        index = overall_index++;

        //get the term
        term = Terms.GetRandomTerm(index, out isCorrect);

        //get the text component (Image component is set externally)
        textComponent = GetComponentInChildren<Text>();

        //get the image on the panel itself
        PanelImage = GetComponent<Image>();

        //refresh the hud
        OnRefreshHUD();
    }

    public void OnRefreshHUD() {
        //if no term, we should disable the whole gameobject
        if (term == null) {
            Image.sprite = null;
            if (textComponent != null)
                textComponent.text = "";
            gameObject.SetActive(false);
            return;
        }

        //set the sprite and the description (maybe needed later)
        Image.sprite = term.Sprite;
        if (textComponent != null)
            textComponent.text = term.Word.ToUpper();

        if (isCorrect)
            textComponent.alignment = TextAnchor.MiddleRight;
    }

    public void OnClick() {
        foreach (Answer a in AllAnswers)
            a.Resolve();
    }

    public void Resolve() {
        PanelImage.color = isCorrect ? Color.green : Color.red;
        PanelImage.enabled = true;

        textComponent.enabled = true;
    }

}
