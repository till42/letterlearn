﻿using UnityEngine;
using UnityEditor;
using System;

public class TermsEditorWindow : EditorWindow {

    [MenuItem("Window/Terms Editor")]
    private static void ShowWindow() {
        GetWindow<TermsEditorWindow>("Terms Editor");
    }

    private Vector2 scrollPos;


    [ExecuteInEditMode]
    private void OnGUI() {

        if (GUILayout.Button("Refresh List")) {
            TermList.RefreshFiles();
        }

    }


}
